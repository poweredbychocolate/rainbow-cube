// pin for rgb module
# define r_led 5
# define g_led 6
# define b_led 3
//button pin
# define pulseButton 9
# define speedButton 11
// pulse count
# define _pulse 4
//current color value
int r = 0;
int g = 0;
int b = 0;
///
bool pulseFlag = false;
// delay time between color change steps 
int delayTab[] = { 1, 5, 10, 20, 30 };
int delayPos = 3; //default delay time posioton in above table 
int delayTabSize = sizeof(delayTab) / sizeof(delayTab[0]);
int _delay = delayTab[delayPos];
///
unsigned long lastMillis = 0;
unsigned long newMillis = 0;
// colors table 
int colors[][3] = { { 255, 0, 0 }, //red
		{ 255, 255, 0 }, //yellow
		{ 0, 255, 0 }, //green
		{ 255, 255, 255 }, //white               
		{ 0, 0, 255 }, //blue
		{ 255, 165, 0 }, //orange               
		{ 160, 3, 255 }, //violet
		};
int black[] = { 0, 0, 0 };
void setup() {
	// set rgb module output
	pinMode(r_led, OUTPUT);
	pinMode(g_led, OUTPUT);
	pinMode(b_led, OUTPUT);
	pinMode(pulseButton, INPUT);
	pinMode(speedButton, INPUT);
	Serial.begin(9600);
}
void loop() {
	Serial.println(delayTabSize);
	// colors table iteration 
	for (int c = 0; c <= sizeof(colors) / sizeof(colors[0]); c++) {
		Serial.println(c);
		if (pulseFlag)
			Serial.println("pulseFlag => true");
		else
			Serial.println("pulseFlag => false");
		if (pulseFlag) {
			for (int b = 0; b < _pulse; b++) {
				setColor(colors[c]);
				setColor(black);
			}
		} else {
			setColor(colors[c]);
		}
		delay(_delay);
	}
}
// change current color to destination color
void setColor(int color[3]) {
//
	while (r != color[0] || g != color[1] || b != color[2]) {
		if (r < color[0])
			r++;
		if (r > color[0])
			r--;

		if (g < color[1])
			g++;
		if (g > color[1])
			g--;

		if (b < color[2])
			b++;
		if (b > color[2])
			b--;

		analogWrite(r_led, r);
		analogWrite(g_led, g);
		analogWrite(b_led, b);

		newMillis = millis();
		if (newMillis - lastMillis > 800) {
			if (digitalRead(pulseButton)) {
				pulseFlag = !pulseFlag;
			}
			if (digitalRead(speedButton)) {
				delayPos = (delayPos + 1) % delayTabSize;
				_delay = delayTab[delayPos];

				Serial.print("delayTime =>");
				Serial.println(delayTab[delayPos]);
			}
			lastMillis = newMillis;
		}
		delay(_delay);
	}
}

